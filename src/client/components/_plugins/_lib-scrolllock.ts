
/*
  Body Scroll Lock
  ------------- 
*/
export { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from '@poemkit/components/_plugins/BSL/lib/bodyScrollLock.es6.js';

