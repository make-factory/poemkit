
/*
  Swiper
  ------------- 
*/
import '@poemkit/components/_plugins/Swiper/src/swiper.scss';

//IMPORT_COMPONENTS
import "@poemkit/components/_plugins/Swiper/src/components/zoom/zoom.scss";
import "@poemkit/components/_plugins/Swiper/src/components/thumbs/thumbs.scss";
import "@poemkit/components/_plugins/Swiper/src/components/scrollbar/scrollbar.scss";
import "@poemkit/components/_plugins/Swiper/src/components/pagination/pagination.scss";
import "@poemkit/components/_plugins/Swiper/src/components/navigation/navigation.scss";
import "@poemkit/components/_plugins/Swiper/src/components/lazy/lazy.scss";
import "@poemkit/components/_plugins/Swiper/src/components/effect-flip/effect-flip.scss";
import "@poemkit/components/_plugins/Swiper/src/components/effect-fade/effect-fade.scss";
import "@poemkit/components/_plugins/Swiper/src/components/effect-cube/effect-cube.scss";
import "@poemkit/components/_plugins/Swiper/src/components/effect-coverflow/effect-coverflow.scss";
import "@poemkit/components/_plugins/Swiper/src/components/core/core.scss";
import "@poemkit/components/_plugins/Swiper/src/components/controller/controller.scss";
import "@poemkit/components/_plugins/Swiper/src/components/a11y/a11y.scss";

export { default as Swiper } from '@poemkit/components/_plugins/Swiper/src/swiper.js';








