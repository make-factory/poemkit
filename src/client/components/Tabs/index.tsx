/* 
 *************************************
 * <!-- Tabs -->
 *************************************
 */
export { default as Tabs } from '@poemkit/components/Tabs/Tabs';
export { default as TabList } from '@poemkit/components/Tabs/TabList';
export { default as TabPanel } from '@poemkit/components/Tabs/TabPanel';