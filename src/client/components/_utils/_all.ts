/**

	TABLE OF CONTENTS FOR `ALL UTILITIES`
	---------------------------

*/

import __ from '@poemkit/components/_utils/scripts/helpers.js';
import { templateUrl, homeUrl, ajaxUrl } from '@poemkit/components/_utils/scripts/global-variables.js';
import { setBG } from '@poemkit/components/_utils/scripts/set-background.js';


export { 
	__,
	templateUrl,
	homeUrl,
	ajaxUrl,
	setBG
};


