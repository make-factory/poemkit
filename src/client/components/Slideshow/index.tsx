/* 
 *************************************
 * <!-- Slideshow -->
 *************************************
 */
export { default as Slideshow } from '@poemkit/components/Slideshow/Slideshow';
export { default as SlideshowItem } from '@poemkit/components/Slideshow/SlideshowItem';