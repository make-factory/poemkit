/* 
 *************************************
 * <!-- Tabs Animated -->
 *************************************
 */
export { default as TabsAnimated } from '@poemkit/components/TabsAnimated/TabsAnimated';
export { default as TabList } from '@poemkit/components/TabsAnimated/TabList';
export { default as TabPanel } from '@poemkit/components/TabsAnimated/TabPanel';